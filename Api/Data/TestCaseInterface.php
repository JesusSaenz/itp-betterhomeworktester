<?php


namespace Gamma\ITP\Api\Data;


interface TestCaseInterface
{
    const ARGUMENTS = 'arguments';
    const EXPECTED_VALUE = 'expected';
    const FUNCTION_NAME = 'function';
    const PASSED = 'passed';
    const RESULT = 'result';

    /**
     * @return array
     */
    public function getArguments(): array;

    /**
     * @param array $args
     * @return TestCaseInterface
     */
    public function setArguments(array $args): TestCaseInterface;

    /**
     * @return mixed
     */
    public function getExpected();

    /**
     * @param $expected mixed
     * @return TestCaseInterface
     */
    public function setExpected($expected): TestCaseInterface;

    /**
     * @return string
     */
    public function getFunctionName(): string;

    /**
     * @param string $functionName
     * @return TestCaseInterface
     */
    public function setFunctionName(string $functionName): TestCaseInterface;

    /**
     * @return bool
     */
    public function getPassed(): bool;

    /**
     * @param bool $passed
     * @return TestCaseInterface
     */
    public function setPassed(bool $passed): TestCaseInterface;

    /**
     * @return mixed
     */
    public function getResult();

    /**
     * @param $result mixed
     * @return TestCaseInterface
     */
    public function setResult($result): TestCaseInterface;
}