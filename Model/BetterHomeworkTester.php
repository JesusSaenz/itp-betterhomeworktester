<?php


namespace Gamma\ITP\Model;


use Exception;
use Gamma\ITP\Api\BetterHomeworkTesterInterface;
use Gamma\ITP\Api\Data\BetterTestCaseInterface;
use Gamma\ITP\Api\Data\TestCaseInterface;
use Gamma\ITP\Api\HomeworkTesterInterface;

class BetterHomeworkTester extends HomeworkTester
{
    public function run($homework, array $testCases): array
    {
        $numTestsPassed = 0;
        $numTests = count($testCases);
        $time0 = microtime(true);
        $testCases = $this->runAllCases($homework, $testCases);
        $time1 = microtime(true);
        $executionTime = $time1 - $time0;
        foreach ($testCases as $testCase) {
            $numTestsPassed += $testCase->getPassed() ? 1 : 0;
        }
        $numTestsFailed = $numTests - $numTestsPassed;
        echo "Execution time: " . sprintf('%.8f', $executionTime) . "s" . "\n";
        echo "# Tests passed: {$numTestsPassed}\n" . "# Tests failed: {$numTestsFailed}\n" . "# Tests: {$numTests}";
        return $testCases;
    }

    public function displayResults(TestCaseInterface $testCase): void
    {
        $actual = $testCase->getResult() == true? "TRUE" : "FALSE";
        $expected = $testCase->getExpected() == true? "TRUE" : "FALSE";
        $args = implode(', ', $testCase->getArguments());
        echo "Test case: {$testCase->getFunctionName()}({$args}): " . ($testCase->getPassed() ? 'PASSED' : 'FAILED') . " Expected: {$expected}, Actual: {$actual}" . "\n";
    }

    protected function runTestCase($homework, TestCaseInterface &$testCase)
    {
        $functionName = $testCase->getFunctionName();
        $result = $homework->$functionName(...$testCase->getArguments());
        $testCase->setPassed($result === $testCase->getExpected())
            ->setResult($result);
        $this->displayResults($testCase);
        echo "-------------------------------------\n";
    }

    protected function handleTestException(TestCaseInterface &$testCase, Exception $exception)
    {
        echo "{$exception}";
        $testCase->setPassed(false)
            ->setResult($exception);
    }

}