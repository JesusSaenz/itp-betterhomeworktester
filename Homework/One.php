<?php


namespace Gamma\ITP\Homework;


class One
{

    function isAnagram($s1, $s2)
    {
        $r1 = str_split($s1);
        $r2 = str_split($s2);

        sort($r1);
        sort($r2);

        return  $r1 == $r2 ? True : False;
    }

}